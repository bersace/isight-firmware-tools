<?php
$dirname = dirname($_SERVER['PHP_SELF']);
$title = "Built-in iSight Firmware Tools";

$readme = wordwrap(file_get_contents("README"));


/* determine wether a file is to be shown or not. */
function show($filename)
{
	$patterns = array("^[#\.]", "~$", "\.php\d?$",
		          "^(?:README|NEWS|HOWTO|resources)");
	$hidden = false;
	foreach($patterns as $pattern) {
		$hidden = $hidden || preg_match("/".$pattern."/", $filename);
	}
	
	return !$hidden;
}

/* scan */
$files = scandir(".");
$files = array_filter($files, "show");
rsort($files);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
      lang="en">
<head>
<title><?php echo $title; ?></title>
<link rel="shortcut icon" type="image/png" href="resources/favicon.png"/>
<link rel="stylesheet" type="text/css" href="resources/style.css"/>
<script type="text/javascript" src="resources/sorttable.js" />
</head>

<body>
<h1><?php echo $title; ?></h1>
<hr/>
<pre><?php echo $readme; ?>

</pre>
<hr/>
<h2>Howto</h2>
<pre><?php readfile('HOWTO'); ?></pre>
<hr/>
<h2>News</h2>
<pre><?php readfile('NEWS'); ?></pre>
<hr>
<h2>Downloads</h2>
<table class="sortable" id="dirlist">
	<thead>
		<tr>
			<th>Name</th>
			<th>Last Modified</th>
			<th>Size</th>
		</tr>
	</thead>
	<tbody>
<?php foreach($files as $file) : ?>
		<tr>
			<td><a href="<?php echo $file; ?>"><?php echo $file; ?></a></td>
			<td><span><?php echo date("d-M-Y H:i", filemtime($file)); ?></span></td>
			<td><span><?php echo round(filesize($file)/1024); ?>k</span></td>
		</tr>
<?php endforeach; ?>
	</tbody>
</table>
<hr>
<p>Last update : <?php echo date("d-M-Y H:i", filemtime(".")); ?></p>
</body>
</html>